colmap (3.10-1) unstable; urgency=medium

  * New upstream version.

 -- Alex Myczko <tar@debian.org>  Wed, 24 Jul 2024 20:58:56 +0000

colmap (3.9.1-3) unstable; urgency=medium

  * Bump standards version to 4.7.0.

 -- Alex Myczko <tar@debian.org>  Tue, 16 Jul 2024 15:15:48 +0000

colmap (3.9.1-2) unstable; urgency=medium

  * Undo paths for configure since it has been fixed, see #1060249.

 -- Gürkan Myczko <tar@debian.org>  Wed, 10 Jan 2024 14:44:18 +0100

colmap (3.9.1-1) unstable; urgency=medium

  * New upstream version.
  * d/control: add libsuitesparse-dev to build-depends.
  * d/rules: add include paths for configure.
  * d/patches/gh-pr-2338: added to fix FTBFS.

 -- Gürkan Myczko <tar@debian.org>  Wed, 10 Jan 2024 07:09:19 +0100

colmap (3.8+git20231101+ds-2) unstable; urgency=medium

  * d/control: add missing b-d libcgal-dev. (Closes: #1055256)

 -- Gürkan Myczko <tar@debian.org>  Fri, 03 Nov 2023 11:30:29 +0100

colmap (3.8+git20231101+ds-1) unstable; urgency=medium

  * New upstream version. (Closes: #1054556)
  * Bump standards version to 4.6.2.
  * d/colmap.install: updated.
  * d/clean: updated.
  * d/copyright: updated.

 -- Gürkan Myczko <tar@debian.org>  Wed, 01 Nov 2023 17:19:40 +0100

colmap (3.8-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright:
    - bump years.
    - drop flann and graclus parts.
  * d/control: add libflann-dev and libsqlite3-dev to build-depends.

 -- Gürkan Myczko <tar@debian.org>  Wed, 01 Feb 2023 07:59:00 +0100

colmap (3.7-3) unstable; urgency=medium

  * d/control: add libmetis-dev to build-depends. (Closes: #1014074)

 -- Gürkan Myczko <tar@debian.org>  Mon, 11 Jul 2022 22:00:15 +0200

colmap (3.7-2) unstable; urgency=medium

  * Add patch that upstream recommends for GUI.

 -- Gürkan Myczko <tar@debian.org>  Wed, 26 Jan 2022 09:19:35 +0100

colmap (3.7-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 3.6.0.
  * d/control: update Vcs urls.
  * d/copyright: bump years.
  * Update maintainer address.
  * README.source: updated.

 -- Gürkan Myczko <tar@debian.org>  Mon, 24 Jan 2022 20:14:44 +0100

colmap (3.6+really3.6+git20210519-2) unstable; urgency=medium

  * Upload to unstable.
  * d/colmap.install: apply patch to fix FTBFS on hurd-i386.
    Thanks Pino Toscano.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 04 Oct 2021 12:14:32 +0200

colmap (3.6+really3.6+git20210519-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 19 May 2021 16:04:27 +0200

colmap (3.6+really3.6-1) unstable; urgency=medium

  * New upstream version. (Closes: #957094)
  * d/rules: run tests. (Closes: #909198)
  * d/upstream/metadata: added.
  * d/control: added Rules-Requires-Root.
  * d/copyright:
    - update copyright years.
    - added Upstream-Contact.
  * Bump debhelper version to 13, drop d/compat.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 01 Sep 2020 12:24:54 +0200

colmap (3.6+dev3+git20200516-1) unstable; urgency=medium

  * New upstream version.
  * Update build dependency. (Closes: #960784)
  * Bump standards version to 4.5.0.
  * Add Vcs fields.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 16 May 2020 20:25:14 +0200

colmap (3.6+dev2+git20191105-1) unstable; urgency=medium

  * New upstream version. (Closes: #925655)
  * d/copyright: updated due to relicensing.
  * Bump standards version to 4.4.1.
  * d/control: drop libcolmap-dev package.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 05 Nov 2019 10:13:22 +0100

colmap (3.6+dev2-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/rules: comment out DDISABLE_CPU_SSE for amd64. (Closes: #915809)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 05 Aug 2019 13:38:48 +0200

colmap (3.6+dev2-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 25 Mar 2019 15:02:35 +0100

colmap (3.6+dev1-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 06 Nov 2018 12:39:38 +0100

colmap (3.5-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.2.1
  * Drop debian/colmap.desktop.
  * Add new libcolmap-dev binary package.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 27 Aug 2018 14:05:23 +0200

colmap (3.4-2) unstable; urgency=medium

  * Add desktop file.
  * Add debian/README.source.
  * Disable AVX/SSE. Thanks to Adrian Bunk for the patch. (Closes: #890106)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 17 Feb 2018 20:18:06 +0100

colmap (3.4-1) unstable; urgency=medium

  * Initial release. (Closes: #882326)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 05 Jan 2018 10:10:47 +0100
